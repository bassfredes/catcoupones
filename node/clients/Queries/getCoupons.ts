import { InstanceOptions, IOContext, JanusClient } from "@vtex/api";

export class GetCoupons extends JanusClient {
    public constructor(ctx: IOContext, options?: InstanceOptions) {
        super(ctx, {
            ...options,
            headers: {
                ...(options && options.headers),
                ...(ctx.authToken
                    ? { VtexIdclientAutCookie: ctx.authToken }
                    : null),
            },
        });
    }
    public async getCoupons() {
        let url = "/api/rnb/pvt/coupon/usage"

        const { data } = await this.http.get(url)

        const response = [
            {
                utmSource: `${data[0].utmSource}`
            }
        ]

        return response;
    }
}

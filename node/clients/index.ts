import { IOClients } from '@vtex/api'
import { GetCoupons } from './Queries/getCoupons'

export class Clients extends IOClients {
    public get getCoupons() {
        return this.getOrSet('getCoupons', GetCoupons)
    }
}
import { getCoupons } from './_getCoupons'

const Query = { getCoupons };

export default Query;
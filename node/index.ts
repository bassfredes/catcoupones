import { Service } from "@vtex/api";
import { Clients } from './clients'

import Query from "./graphql/Queries/Resolver";

export default new Service({
	clients: {
		implementation: Clients,
		options: {
			getCoupons: {
				timeout: 10000
			}
		}
	},
	graphql: {
		resolvers: {
			Query
		}
	},
	routes: {
		robots: async ctx => {
			const { response: res } = ctx;
			res.set("Content-Type", "text/plain");
			res.body = "User-agent:*\nDisallow:/all";

			return (res.status = 200);
		}
	}
});
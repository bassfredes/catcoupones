# IO Clients

Clients is the most recommended way to make requests with `NodeJS` and `GraphQL` in VTEX IO.

> You'll need to have an app with `node` and `graphql` builders, you can request your app and its builders [here](https://vtex.io/docs/releases/2020-02/vtex-io-closed-beta-list).

You can use this repository as a template to start your development, here you can find `query` and `mutation` VTEX Master Data requests examples.

## Steps to make an client request

### Step 1: Declaring your query or mutation

Go to `graphql/schema.graphql` and create your query or mutation.\
Like:

```
type Query {
    myQuery(
      queryVariable1: String
      queryVariable2: Boolean
    ): QueryResponse @cacheControl(scope: PRIVATE)
}

type Mutation {
    myMutation(
      mutat ionVariable1: String
      mutationVariable2: Boolean
    ): MutationResponse @cacheControl(scope: PRIVATE)
}

type QueryResponse {
    status: Int
}

type MutationResponse {
    status: Int
}
```

### Step 2: Declaring your request host on the manifest.json policies
You need to add your request host in manifest.json policies.\
Like:
```
{
    "policies": [
        {
            "name": "outbound-access",
            "attrs": {
                "host": "{{account}}.vtexcommercestable.com.br",
                "path": "/api/*"
            }
        }
    ],
}
```
### Step 3: Creating your request

Go to `node/clients/Mutations` or `node/clients/Queries` and create your request file.\
Like: `node/clients/Queries/myQuery.ts`

File contents example:

```
import { InstanceOptions, IOContext, ExternalClient } from "@vtex/api";

export class MyQuery extends ExternalClient {
    public constructor(ctx: IOContext, options?: InstanceOptions) {
        super("http://licensemanager.vtex.com.br/api/pvt/accounts", ctx, {
            ...options,
            headers: {
                ...(options && options.headers),
                ...(ctx.adminUserAuthToken
                    ? { VtexIdclientAutCookie: ctx.adminUserAuthToken }
                    : null)
            },
        });
    }

    public async myQuery(queryVariable1: String, queryVariable2: Boolean) {
        const request = await this.http.get(`https://corebiz.vtexcommercestable.com.br/api/dataentities/IC/search?_fields=_all_&_where=whereField1=${queryVariable1} AND whereField2=${queryVariable2}`)

        return request
    }
}
```

### Step 4: Declaring your request to clients
Go to `node/clients/index.ts` and declare your request.\
Like:

```
import { IOClients } from '@vtex/api'
import { MyQuery } from './Queries/myQuery.ts'

export class Clients extends IOClients {
    ...
    public get myQuery() {
        return this.getOrSet('myQuery', MyQuery)
    }
}

```

### Step 5: Creating your resolver
Go to `node/graphql/Mutations` or `node/graphql/Queries` and create your resolver file.\
Like: `node/graphql/Queries/_myQuery.ts`

File contents example:
```
const myQuery = async (_obj: any, args: any, ctx: any, _info: any) => {
    const { clients } = ctx;

    ctx.set("Cache-Control", "no-cache, no-store")

    const request = await clients.myQuery.myQuery(args.queryVariable1, args.queryVariable2)

    return request
};

export { myQuery };

```

### Step 6: Importing your resolver
Go to `node/graphql/Queries/Resolver.ts` and add your resolver.\
Like:

```
import { myQuery } from './_myQuery'

const Query = {
    myQuery,
    ...
};

export default Query;
```

### Step 7: Testing your query
- Run `vtex link` in your workspace.
- Go to `https://{workspace}--{account}.myvtex.com/_v/private/{vendor}.{appName}@{0.0.0}/graphiql/v1`.
- Make your tests.